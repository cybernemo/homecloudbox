CREATE DATABASE IF NOT EXISTS nextcloud;
CREATE USER 'nextclouduser'@'%' IDENTIFIED BY 'password';
GRANT ALL ON nextcloud.* TO 'nextclouduser'@'%' IDENTIFIED BY 'password' WITH GRANT OPTION;
FLUSH PRIVILEGES;