CREATE DATABASE IF NOT EXISTS onlyoffice;
CREATE USER 'onlyofficeuser'@'%' IDENTIFIED BY 'password';
GRANT ALL ON nextcloud.* TO 'onlyofficeuser'@'%' IDENTIFIED BY 'password' WITH GRANT OPTION;
FLUSH PRIVILEGES;