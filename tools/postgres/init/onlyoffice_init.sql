CREATE DATABASE onlyoffice;
CREATE USER onlyofficeuser WITH ENCRYPTED PASSWORD 'password';
GRANT ALL PRIVILEGES ON DATABASE onlyoffice TO onlyofficeuser;