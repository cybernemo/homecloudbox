# HomeCloudBox

HomeCloudBox is a toolbox project aimed to bring the cloud back home by providing an open-source alternative to popular services like cloud storage, online office suite and much more. All services are running on docker containers.

## Table of Content

- [HomeCloudBox](#homecloudbox)
  - [Table of Content](#table-of-content)
  - [Introduction](#introduction)
  - [Installation](#installation)
  - [Applications](#applications)
    - [Bitwarden](#bitwarden)
    - [Code Server](#code-server)
    - [Firefox SyncServer](#firefox-syncserver)
    - [Nextcloud](#nextcloud)
    - [OnlyOffice](#onlyoffice)
  - [Tools](#tools)
    - [Portainer](#portainer)
    - [Watchtower](#watchtower)
  - [TODO](#todo)
    - [General](#general)
    - [Bitwarden](#bitwarden-1)
    - [Code Server](#code-server-1)
    - [Firefox](#firefox)
    - [Mysql](#mysql)
    - [Nextcloud](#nextcloud-1)
    - [Onlyoffice](#onlyoffice-1)
    - [Watchtower](#watchtower-1)

## Introduction

The current available applications are:

- [Bitwarden](#bitwarden)
- [Code Server](#code-server)
- [Firefox SyncServer](#firefox-syncserver)
- [NextCloud](#nextcloud)
- [OnlyOffice](#onlyoffice)

Current supported OSes:

- [x] Ubuntu 20.04
- [ ] Rasperry pi OS (64bit)
- [ ] CentOS 7

## Installation

To run the installer run the following:

```bash
./install.sh
```

## Applications

List of the current supported applications.

| Application         | Network   | port |
| ------------------- | --------- | ---- |
| Bitwarden           | proxy     | 8081 |
| Nextcloud           | proxy, db | 8082 |
| Code Server         | proxy     | 8083 |
| OnlyOffice          | proxy, db | 8084 |
| PHPMyAdmin          | proxy, db | 8090 |
| Adminer             | proxy, db | 8091 |
| Firefox Sync Server | proxy     |      |
| MariaDB             | db        | 3306 |
| Postgres            | db        | 5432 |
| Portainer           | proxy     | 8000 |



### [Bitwarden](./apps/compose-bitwarden.yml)

Bitwarden is a self-hosted application to managed password.

Official web site: https://bitwarden.com/  
Docker image: https://hub.docker.com/r/bitwardenrs/server

### [Code Server](./apps/compose-codeserver.yml)

Code Server is a web IDE of Code OSS, the open-source version of vSCode.

Official web site: https://appimage.github.io/Code_OSS  
Docker image: https://hub.docker.com/r/linuxserver/code-server

### [Firefox SyncServer](./apps/compose-syncserver.yml)

Firefox SyncServer is a self-hosted server for syncing bookmarks and passwords for the Firefox web browser.

Official web site: https://github.com/mozilla-services/syncserver  
Docker image: https://hub.docker.com/r/mozilla/syncserver

### [Nextcloud](./apps/compose-nextcloud.yml)

NextCloud is an open-source cloud storage alternative to Dropbox, Google Drive,...

Official web site: https://nextcloud.com/  
Docker image: https://hub.docker.com/_/nextcloud

### [OnlyOffice](./apps/compose-onlyoffice.yml)

OnlyOffice is an open-source Office suite.

Official web site: https://www.onlyoffice.com/  
Docker images: https://hub.docker.com/r/onlyoffice/documentserver and https://hub.docker.com/r/onlyoffice/communityserver

## Tools

List of the current tools used for managing the applications.

### [Portainer](./tools/compose-portainer.yml)

Portainer is an open-source container management tool.

Official web site: https://www.portainer.io/  
Docker image: https://hub.docker.com/r/portainer/portainer

### [Watchtower](./tools/compose-watchtower.yml)

Watchtower is an tool to automatically update your containers.

Official web site: https://containrrr.dev/watchtower/  
Docker image: https://hub.docker.com/r/v2tec/watchtower

## TODO

### General

- [x] ~~Add non-containerized mySQL for Nextcloud and OnlyOffice~~=> used one mariadb container
- [ ] use secret for passwords
- [ ] add nginx or traefik for name resolution

### Bitwarden

### Code Server

### Firefox

- [ ] create compose file

### Mysql

- [ ] set database password (root and nextcloud/onlyoffice) by user entries

### Nextcloud

- [ ] create install dialog to configure nextcloud

### Onlyoffice

- [ ] create compose file

### Watchtower

- [ ] add notification when any container image is updated
